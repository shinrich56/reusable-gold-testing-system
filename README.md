Reusable Gold Testing System
===================================

Reusable Gold testing system, or autest for short, is a testing system targeted toward gold file, command line process testing.

Goals
----------------------------------

* Easy to write and add tests
* Extensible system to allow: 
 * Adding new functionality for testing your application
 * Batch commands as a new function to make it easier to write tests
 * Define custom report outputs
* Precise as possible error messages to make it easy to see what is wrong fast
* Sandbox to make it easy to see what failed and reproduce out of test system
* Flexible gold file synatx to make it easier to ignore text that is not important
* Run on python 2 or 3

Non-Goals
----------------------------------

The Reusable Gold testing system is not about making another unit testing framework.

How do I get set up?
----------------------------------
Make sure you have python installed

To install run the command to get latest published version:
~~~~
 pip install autest
~~~~

To install the latest development version, run the command:
~~~~
pip install git+https://bitbucket.org/autestsuite/reusable-gold-testing-system.git
~~~~



Basic usage
----------------------------------

In the directory containing the tests run:
~~~~
autest
~~~~
[Quick start for creating a test](https://bitbucket.org/dragon512/reusable-gold-testing-system/wiki/quick-start)

Read [documentation](https://autestsuite.bitbucket.io/) to learn how to use write and use AuTest in more detail ( docs are being rewritten )
[here.](https://autestsuite.bitbucket.io/)

Contribution guidelines
----------------------------------

* Feel free to suggest fixes or ask question
* Have a fix submit a pull request!
